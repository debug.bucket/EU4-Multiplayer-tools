﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

GroupAdd, Group, ahk_exe eu4.exe
GroupAdd, Group, ahk_exe hoi4.exe
GroupAdd, Group, ahk_exe CK2game.exe

#IfWinActive, ahk_group Group

w::Send {Up DOWN}
w UP::Send {Up UP}
+w::Send w

a::Send {Left DOWN}
a UP::Send {Left UP}
+a::Send a

s::Send {Down DOWN}
s UP::Send {Down UP}
+s::Send s

d::Send {Right DOWN}
d UP::Send {Right UP}
+d::Send d

^Space::
Suspend, Toggle
If %A_IsSuspended%
  SoundPlay, %WINDIR%\media\close_window.wav
Else
  SoundPlay, %WINDIR%\media\close_window.wav
Return