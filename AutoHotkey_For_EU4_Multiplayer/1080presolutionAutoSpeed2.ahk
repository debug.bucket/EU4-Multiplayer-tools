#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input

/*
Made by: Jakob
discord: Jakob#6989
E-mail: debug.bucket@gmail.com
*/

GroupAdd, Group, ahk_exe eu4.exe

CoordMode, Pixel, Window

msgbox,F5 = stop autospeed `nF6 = start autospeed `nWindowsKey+F2 = close script entirely

#F2::
exitApp

#IfWinActive, ahk_group Group


F2::
MouseGetPos px,py
PixelGetColor, Color, %px%, %py%, RGB
MsgBox, Pixel color at %px%, %py% is %Color%
Return

F5::
SoundPlay, %A_ScriptDir%\close_window.wav
stop := 1
return

F6::
stop := 0
SoundPlay, %A_ScriptDir%\close_window.wav
Loop
{
	if (stop > 0)
		return
	PixelGetColor, Color, 1887, 54, RGB
	If (Color = 0x1B2125)
	    Send {+}
	Sleep 5000
}
return