﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input

GroupAdd, Group, ahk_exe eu4.exe

CoordMode, Pixel, Screen

#IfWinActive, ahk_group Group

^z::
SoundPlay, %A_ScriptDir%\close_window.wav

;Alt|Slow|

PixelGetColor, colorCheck2, 3777, 111, , alt slow RGB
PixelGetColor, colorCheck3, 3757, 100, , alt slow RGB

MsgBox, 4, , SPEED 2 ACTIVATED - colorCheck2: %colorCheck2% `nSpeed 3 activated - colorCheck3: %colorCheck3%
IfMsgBox No
    Exit
    
;speed 2 de-activated Color:262D33(Red=26 Green=2D Blue=33)
PixelSearch, Px, Py, X3777, Y111, X3777, Y111, 0x641516, 255, RGB
if ErrorLevel
    MsgBox, SPEED 2 DE-ACTIVATED: That color was not found in the specified region.
else
    MsgBox, SPEED 2 DE-ACTIVATED: A color within 5 shades of variation was found at X%Px% Y%Py%.

;Speed 3 de-activated Color:792627 (Red=79 Green=26 Blue=27)
PixelSearch, Px, Py, x3757, Y100, x3757, Y100, 0x600d0e, 55, RGB
if ErrorLevel
    MsgBox, SPEED 3 DE-ACTIVATED: That color was not found in the specified region.
else
    MsgBox, SPEED 3 DE-ACTIVATED: A color within 5 shades of variation was found at X%Px% Y%Py%.

;speed 2 activated Color:262D33(Red=26 Green=2D Blue=33)
PixelSearch, Px, Py, X3777, Y111, X3777, Y111, 0x262D33, 55, RGB
if ErrorLevel
    MsgBox, SPEED 2 DE-ACTIVATED: That color was not found in the specified region.
else
    MsgBox, SPEED 2 DE-ACTIVATED: A color within 5 shades of variation was found at X%Px% Y%Py%.

;Speed 3 activated Color:792627 (Red=79 Green=26 Blue=27)
PixelSearch, Px, Py, x3757, Y100, x3757, Y100, 0x283239, 55, RGB
if ErrorLevel
    MsgBox, SPEED 3 DE-ACTIVATED: That color was not found in the specified region.
else
    MsgBox, SPEED 3 DE-ACTIVATED: A color within 5 shades of variation was found at X%Px% Y%Py%.

exit
/*
w::Send {NumpadAdd}
w UP::Send {NumpadSub}
*/

^x::  ; Control+Alt+Z hotkey.
MouseGetPos, MouseX, MouseY
PixelGetColor, color, %MouseX%, %MouseY%
MsgBox The color at the current cursor position is %color%.
return

^Space::
Suspend, Toggle
If %A_IsSuspended%
  SoundPlay, %A_ScriptDir%\close_window.wav
Else
  SoundPlay, %A_ScriptDir%\close_window.wav
Return
